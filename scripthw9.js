function ItemsToDOM(array, parent = document.body) {
    const ul = document.createElement('ul');
    for (const item of array) {
      const li = document.createElement('li');
      li.textContent = item;
      ul.appendChild(li);
    }
    parent.appendChild(ul);
  }
  
const myArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 'adadad'];
ItemsToDOM(myArray);